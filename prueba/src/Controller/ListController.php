<?php

namespace App\Controller;

use App\Entity\Libro;
use App\Repository\LibroRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\Persistence\ManagerRegistry;

class ListController extends AbstractController
{
    /**
     * @Route("/list", name="app_list")
     */
    public function index(LibroRepository $libroRepository): Response
    {
        return $this->render('list/index.html.twig', [
            'libros' => $libroRepository->findAll(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="app_delete")
     */
    public function delete(Libro $libro, ManagerRegistry $doctrine): RedirectResponse
    {
        $em = $doctrine->getManager();
        $em->remove($libro);
        $em->flush();
        return $this->redirectToRoute('app_list');
    }
}
