<?php

namespace App\DataFixtures;

use App\Entity\Libro;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for($i = 0; $i<5; $i++){
            $libro = new Libro();
            $libro->setTitulo("Titulo ".strval($i));
            $libro->setDescripcion("Descripcion ".strval($i));
            $libro->setAnio("202".strval($i));
            $libro->setAutor("Autor ".strval($i));;
            $manager->persist($libro);
            $manager->flush();
        }
    }
}
